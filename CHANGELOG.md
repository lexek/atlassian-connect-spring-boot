# Changelog

All notable changes to this project will be documented in this file.

## 2.2.1 - 2021-06-25

- [ACSPRING-135](https://ecosystem.atlassian.net/browse/ACSPRING-135) App installation not working due to base64 issues on Windows

## 2.2.0 - 2021-06-18

- [ACSPRING-131](https://ecosystem.atlassian.net/browse/ACSPRING-131) Support installation hooks signed with asymmetric key

## 2.1.9 - 2021-06-11

- [ACSPRING-132](https://ecosystem.atlassian.net/browse/ACSPRING-132) Allow initial install lifecycle callbacks with new clientKey and same baseUrl

## 2.1.8 - 2021-05-31

- [ACSPRING-125](https://ecosystem.atlassian.net/browse/ACSPRING-125) Error in JWT parsing with Spring Boot 2.4.3+

## 2.1.5 - 2021-05-06

- [ACSPRING-126](https://ecosystem.atlassian.net/browse/ACSPRING-126) QSH Validation triggering

## 2.1.4 - 2021-03-25

- [ACSPRING-124](https://ecosystem.atlassian.net/browse/ACSPRING-124) Add ContextJWT annotation to mark an endpoint as compatible with Context JWTs

## 2.1.3 - 2021-03-21

- [ACSPRING-123](https://ecosystem.atlassian.net/browse/ACSPRING-123) Enforce presence of qsh claim on lifecycle methods

## 2.1.2 - 2020-11-18

- [ACSPRING-121](https://ecosystem.atlassian.net/browse/ACSPRING-121) Deprecate host look-up by base URL
- [ACSPRING-120](https://ecosystem.atlassian.net/browse/ACSPRING-120) Upgrade to Spring Boot 2.4
- [ACSPRING-119](https://ecosystem.atlassian.net/browse/ACSPRING-119) Upgrade to Nimbus JOSE + JWT 9
- [ACSPRING-118](https://ecosystem.atlassian.net/browse/ACSPRING-118) Unsigned installation can introduce base URL conflict

## 2.1.0 - 2020-06-17

- [ACSPRING-117](https://ecosystem.atlassian.net/browse/ACSPRING-117) Host lookup by base URL is not deterministic
- [ACSPRING-116](https://ecosystem.atlassian.net/browse/ACSPRING-117) Update to Spring Security OAuth 2.5
- [ACSPRING-115](https://ecosystem.atlassian.net/browse/ACSPRING-115) Upgrade to Spring Boot 2.3
- [ACSPRING-114](https://ecosystem.atlassian.net/browse/ACSPRING-114) Upgrade to Nimbus JOSE + JWT 8

## 2.0.7 - 2020-05-14

- [ACSPRING-110](https://ecosystem.atlassian.net/browse/ACSPRING-110) Enable HTTP Strict Transport Security
- [ACSPRING-109](https://ecosystem.atlassian.net/browse/ACSPRING-109) Shorten expiration time for OAuth 2.0 JWT Bearer access tokens to accommodate clock skew

## 2.0.6 - 2020-05-04

- [ACSPRING-108](https://ecosystem.atlassian.net/browse/ACSPRING-108) Disable session cookies

## 2.0.5 - 2020-04-08

- [ACSPRING-107](https://ecosystem.atlassian.net/browse/ACSPRING-107) Enable configuring paths that require JWT authentication

## 2.0.4 - 2020-03-02

- [ACSPRING-106](https://ecosystem.atlassian.net/browse/ACSPRING-106) Upgrade to Spring Boot 2.2.4
- [ACSPRING-105](https://ecosystem.atlassian.net/browse/ACSPRING-105) Use atlassian.com URL for OAuth 2.0 JWT authorization server

## 2.0.3 - 2019-12-17

- [ACSPRING-101](https://ecosystem.atlassian.net/browse/ACSPRING-101) Error installing app with Jackson configured to reject additional properties
- [ACSPRING-102](https://ecosystem.atlassian.net/browse/ACSPRING-102) Set Referrer-Policy response header by default

## 2.0.2 - 2019-11-22

- [ACSPRING-98](https://ecosystem.atlassian.net/browse/ACSPRING-98) Upgrade to Spring Security OAuth2 2.4
- [ACSPRING-100](https://ecosystem.atlassian.net/browse/ACSPRING-100) Upgrade to Spring Boot 2.2

## 2.0.1 - 2019-08-30

- [ACSPRING-97](https://ecosystem.atlassian.net/browse/ACSPRING-97) Load all.js from CDN instead of from host product

## 2.0.0 - 2019-08-20

- [ACSPRING-96](https://ecosystem.atlassian.net/browse/ACSPRING-96) Disable X-Frame-Options header by default

## 2.0.0-beta-1 - 2019-06-28

- [ACSPRING-70](https://ecosystem.atlassian.net/browse/ACSPRING-70)	Upgrade to Spring Boot 2.0
- [ACSPRING-89](https://ecosystem.atlassian.net/browse/ACSPRING-89)	Add AtlassianHostRestClients#authenticatedAsAddon(AtlassianHost)	
- [ACSPRING-91](https://ecosystem.atlassian.net/browse/ACSPRING-91)	Upgrade to Nimbus JOSE + JWT 7	
- [ACSPRING-92](https://ecosystem.atlassian.net/browse/ACSPRING-92)	Remove Apache Commons Lang dependency	
- [ACSPRING-93](https://ecosystem.atlassian.net/browse/ACSPRING-93)	Remove Actuator base path property override	
- [ACSPRING-94](https://ecosystem.atlassian.net/browse/ACSPRING-94)	Stop providing JWT-signing RestTemplate as @Component	

## 1.5.1 - 2018-11-29

- [ACSPRING-87](https://ecosystem.atlassian.net/browse/ACSPRING-87) Make archetype add-on opted-in to GDPR

## 1.5.0 - 2018-09-04

- [ACSPRING-81](https://ecosystem.atlassian.net/browse/ACSPRING-81) Use GDPR-compliant APIs when opted in

## 1.4.3 - 2018-02-28

- [ACSPRING-78](https://ecosystem.atlassian.net/browse/ACSPRING-78) Error installing add-on using Hibernate on cloud instance with 2048 bit public key

## 1.4.2 - 2018-02-21

- [ACSPRING-77](https://ecosystem.atlassian.net/browse/ACSPRING-77) Error installing add-on using Liquibase on cloud instance with 2048 bit public key

## 1.4.0 - 2018-02-05

- [ACSPRING-75](https://ecosystem.atlassian.net/browse/ACSPRING-75) Upgrade to nimbus-jose-jwt 4.x

## 1.3.6 - 2018-01-31

- [ACSPRING-65](https://ecosystem.atlassian.net/browse/ACSPRING-65) Improve reporting for start-up failure due to missing AtlassianHostRepository bean
- [ACSPRING-66](https://ecosystem.atlassian.net/browse/ACSPRING-66) Miscellaneous archetype improvements
- [ACSPRING-68](https://ecosystem.atlassian.net/browse/ACSPRING-68) Improve logging for subsequent installation request
- [ACSPRING-73](https://ecosystem.atlassian.net/browse/ACSPRING-73) Asynchronous controller methods return Unauthorized

## 1.3.5 - 2017-07-31

- [ACSPRING-62](https://ecosystem.atlassian.net/browse/ACSPRING-62) Add index for column base_url in table atlassian_host

## 1.3.4 - 2017-07-17

- [ACSPRING-61](https://ecosystem.atlassian.net/browse/ACSPRING-61) Improve log message for JWT signature mismatch

## 1.3.3 - 2017-06-22

- [ACSPRING-60](https://ecosystem.atlassian.net/browse/ACSPRING-60) Allow disabling the redirection from / to /atlassian-connect.json

## 1.3.2 - 2017-05-23

- [ACSPRING-55](https://ecosystem.atlassian.net/browse/ACSPRING-55) Installation details are stored even when the Atlassian host closes the connection due to a timeout
- [ACSPRING-57](https://ecosystem.atlassian.net/browse/ACSPRING-57) Use saved host when firing application event for installation
- [ACSPRING-58](https://ecosystem.atlassian.net/browse/ACSPRING-58) Upgrade to Spring Boot 1.5.3

## 1.3.1 - 2017-02-17

- [ACSPRING-50](https://ecosystem.atlassian.net/browse/ACSPRING-50) Enable use of RestTemplateCustomizers
- [ACSPRING-51](https://ecosystem.atlassian.net/browse/ACSPRING-51) Improve console logging
- [ACSPRING-53](https://ecosystem.atlassian.net/browse/ACSPRING-53) Pluses in query parameters are not decoded in canonical request calculation for outbound requests

## 1.3.0 - 2017-01-23

- [ACSPRING-30](https://ecosystem.atlassian.net/browse/ACSPRING-30) AddonDescriptorController does not honor Spring Boot's contextPath
- [ACSPRING-31](https://ecosystem.atlassian.net/browse/ACSPRING-31) atlassian-connect-* model attributes cannot be used with Thymeleaf
- [ACSPRING-42](https://ecosystem.atlassian.net/browse/ACSPRING-42) Error when using injected RestTemplate with an HttpEntity containing an Authorization header
- [ACSPRING-45](https://ecosystem.atlassian.net/browse/ACSPRING-45) RestTemplate sets JWT subject claim
- [ACSPRING-46](https://ecosystem.atlassian.net/browse/ACSPRING-46) Obtain JWT for making request to given host URL
- [ACSPRING-47](https://ecosystem.atlassian.net/browse/ACSPRING-47) Send User-Agent header when using OAuth 2.0
- [ACSPRING-49](https://ecosystem.atlassian.net/browse/ACSPRING-49) Obtain OAuth 2.0 access token for making request to host

## 1.2.1 - 2017-01-05

- [ACSPRING-43](https://ecosystem.atlassian.net/browse/ACSPRING-43) Use the principal of the Authentication object to determine successful authentication

## 1.2.0 - 2016-12-23

- [ACSPRING-8](https://ecosystem.atlassian.net/browse/ACSPRING-8) Support authenticating requests from iframe content back to the add-on
- [ACSPRING-24](https://ecosystem.atlassian.net/browse/ACSPRING-24) Accept JWT self-authentication tokens
- [ACSPRING-33](https://ecosystem.atlassian.net/browse/ACSPRING-33) Query parameters are double-encoded in canonical request calculation for outbound requests
- [ACSPRING-36](https://ecosystem.atlassian.net/browse/ACSPRING-36) Configure the order of the JWT authentication filter
- [ACSPRING-37](https://ecosystem.atlassian.net/browse/ACSPRING-37) Enable configuration of JWT expiration time
- [ACSPRING-39](https://ecosystem.atlassian.net/browse/ACSPRING-39) Upgrade to Spring Boot 1.4.2
- [ACSPRING-40](https://ecosystem.atlassian.net/browse/ACSPRING-40) Miscellaneous improvements to integration tests
- [ACSPRING-41](https://ecosystem.atlassian.net/browse/ACSPRING-41) Miscellaneous archetype improvements

## 1.1.0 - 2016-11-01

- [ACSPRING-18](https://ecosystem.atlassian.net/browse/ACSPRING-18) Create integration tests using Spring Data MongoDB
- [ACSPRING-22](https://ecosystem.atlassian.net/browse/ACSPRING-22) Support OAuth 2 authentication with JWT client credentials
- [ACSPRING-29](https://ecosystem.atlassian.net/browse/ACSPRING-29) Include spring-boot:repackage in build for archetype add-on

## 1.0.0 - 2016-07-29

- [ACSPRING-7](https://ecosystem.atlassian.net/browse/ACSPRING-7) Fire application events on completion of lifecycle callbacks
- [ACSPRING-25](https://ecosystem.atlassian.net/browse/ACSPRING-25) Add-on uninstalled endpoint returns 400

## 1.0.0-beta-3 - 2016-07-18

- [ACSPRING-17](https://ecosystem.atlassian.net/browse/ACSPRING-17) Steps for modifying an existing spring-boot project are insufficient
- [ACSPRING-19](https://ecosystem.atlassian.net/browse/ACSPRING-19) JWT token header gets added multiple times when using RestTemplate and custom interceptors
