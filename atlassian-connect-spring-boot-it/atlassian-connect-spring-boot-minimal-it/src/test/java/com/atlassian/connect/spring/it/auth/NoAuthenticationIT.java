package com.atlassian.connect.spring.it.auth;

import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.URI;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, properties = "atlassian.connect.require-auth-exclude-paths=/no-auth")
public class NoAuthenticationIT extends BaseApplicationIT {

    @Test
    public void shouldUnauthenticatedAcceptRequestToSecurityIgnoredPath() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(URI.create(getServerAddress() + "/no-auth"), String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody(), is("No authentication required"));
    }

    @TestConfiguration
    public static class PublicControllerConfiguration {

        @Bean
        public PublicController publicController() {
            return new PublicController();
        }
    }

    @Controller
    public static class PublicController {

        @GetMapping(value = "/no-auth", produces = "application/json")
        @ResponseBody
        public String noAuth() {
            return "No authentication required";
        }
    }
}
