package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.request.jwt.JwtBuilder;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.SimpleJwtSigningRestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.SHARED_SECRET;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static com.atlassian.connect.spring.it.util.LifecycleBodyHelper.createLifecycleJson;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LifecycleControllerJwtVerificationIT extends BaseApplicationIT {

    @Test
    public void shouldAcceptInstallForJwtWithQsh() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        ResponseEntity<Void> response = postLifecycle(new SimpleJwtSigningRestTemplate(host, Optional.empty()), "installed");
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
    }

    @Test
    public void shouldRejectInstallForJwtWithoutQsh() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        String jwt = new JwtBuilder().issuer(host.getClientKey()).signature(host.getSharedSecret()).build();
        ResponseEntity<Void> response = postLifecycle(new SimpleJwtSigningRestTemplate(jwt), "installed");
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    @Test
    public void shouldAcceptUninstallForJwtWithQsh() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        ResponseEntity<Void> response = postLifecycle(new SimpleJwtSigningRestTemplate(host, Optional.empty()), "uninstalled");
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
    }

    @Test
    public void shouldRejectUninstallForJwtWithoutQsh() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        String jwt = new JwtBuilder().issuer(host.getClientKey()).signature(host.getSharedSecret()).build();
        ResponseEntity<Void> response = postLifecycle(new SimpleJwtSigningRestTemplate(jwt), "uninstalled");
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    private ResponseEntity<Void> postLifecycle(TestRestTemplate restTemplate, String eventType) throws JsonProcessingException {
        return restTemplate.exchange(RequestEntity.post(getRequestUri(eventType))
                .contentType(MediaType.APPLICATION_JSON)
                .body(createLifecycleJson(eventType, SHARED_SECRET)), Void.class);
    }

    private URI getRequestUri(String path) {
        return UriComponentsBuilder.fromUri(URI.create(getServerAddress())).path(path).build().toUri();
    }
}
