package com.atlassian.connect.spring.it;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LegacyInstallHookAddonApplication {

    public static void main(String[] args) throws Exception {
        new SpringApplication(LegacyInstallHookAddonApplication.class).run(args);
    }
}
