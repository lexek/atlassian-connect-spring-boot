package com.atlassian.connect.spring.it.auth.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricPublicKeyProvider;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import com.atlassian.connect.spring.it.util.AtlassianHosts;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.LifecycleBodyHelper;
import com.atlassian.connect.spring.it.util.SimpleJwtSigningRestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.nimbusds.jose.JOSEException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LegacyInstallJWTVerificationIT extends BaseApplicationIT {

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Autowired
    private AtlassianConnectProperties properties;

    @Autowired
    AsymmetricPublicKeyProvider asymmetricPublicKeyProvider;

    private static final URI PRODUCTION_INSTALL_KEYS_URL = URI.create("https://example.keys.com/");

    @Before
    public void before() {
        properties.setPublicKeyBaseUrl(PRODUCTION_INSTALL_KEYS_URL.toString());
        properties.setAllowSymmetricAuthInstallCallback(true);
        hostRepository.deleteAll();
    }

    @Test
    public void shouldCreateHostFromInstalledEvent() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        TestRestTemplate restTemplate = new SimpleJwtSigningRestTemplate(host, Optional.empty());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertEquals(this.hostRepository.count(), 1);
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertEquals(insertedHost.getClientKey(), host.getClientKey());
    }

    @Test
    public void shouldFallBackOnUninstall() throws JsonProcessingException {
        String clientKey = AtlassianHosts.CLIENT_KEY;
        String secret = "secretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecret";
        AtlassianHost host = new AtlassianHostBuilder().clientKey(clientKey).sharedSecret(secret).build();
        hostRepository.save(host);
        TestRestTemplate restTemplate = new SimpleJwtSigningRestTemplate(host, Optional.empty());
        String unInstalledJson = LifecycleBodyHelper.createLifecycleJson("uninstalled", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(unInstalledJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getUninstalledURI(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertEquals(this.hostRepository.count(), 1);
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertEquals(insertedHost.isAddonInstalled(), false);
        List<String> unexpectedHookHeader = response.getHeaders().get("x-unexpected-symmetric-hook");
        assertNull(unexpectedHookHeader);
    }

    @Test
    public void shouldRejectSecondUnsignedInstall() throws JsonProcessingException {
        String clientKey = AtlassianHosts.CLIENT_KEY;
        String secret = "secretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecret";
        AtlassianHost host = new AtlassianHostBuilder().clientKey(clientKey).sharedSecret(secret).build();
        hostRepository.save(host);
        TestRestTemplate restTemplate = new TestRestTemplate();
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    @Test
    public void shouldNotUseSymmetricSignaturesForSecondInstallAndNotSetUnexpectedHookHeaderIfSignedInstallIsDisabled() throws JOSEException, JsonProcessingException {
        String clientKey = AtlassianHosts.CLIENT_KEY;
        String secret = "secretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecret";
        AtlassianHost host = new AtlassianHostBuilder().clientKey(clientKey).sharedSecret(secret).build();
        hostRepository.save(host);
        TestRestTemplate restTemplate = new SimpleJwtSigningRestTemplate(host, Optional.empty());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertEquals(1, this.hostRepository.count());
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertEquals(insertedHost.getClientKey(), host.getClientKey());
        List<String> unexpectedHookHeader = response.getHeaders().get("x-unexpected-symmetric-hook");
        assertNull(unexpectedHookHeader);
    }

    @Test
    public void shouldThrowServerErrorIfBothSignedInstallAndFallbackConfigsAreDisabled() throws JOSEException, JsonProcessingException {
        properties.setAllowSymmetricAuthInstallCallback(false);
        String clientKey = AtlassianHosts.CLIENT_KEY;
        String secret = "secretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecret";
        AtlassianHost host = new AtlassianHostBuilder().clientKey(clientKey).sharedSecret(secret).build();
        hostRepository.save(host);
        TestRestTemplate restTemplate = new SimpleJwtSigningRestTemplate(host, Optional.empty());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
        assertEquals(1, this.hostRepository.count());
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertEquals(insertedHost.getClientKey(), host.getClientKey());
        List<String> unexpectedHookHeader = response.getHeaders().get("x-unexpected-symmetric-hook");
        assertNull(unexpectedHookHeader);
    }

    private URI getInstalledUri() {
        return UriComponentsBuilder.fromUri(URI.create(getServerAddress())).path("/installed").build().toUri();
    }

    private URI getUninstalledURI() {
        return UriComponentsBuilder.fromUri(URI.create(getServerAddress())).path("/uninstalled").build().toUri();
    }

}
