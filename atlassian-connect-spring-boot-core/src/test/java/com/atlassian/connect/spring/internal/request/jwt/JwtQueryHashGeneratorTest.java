package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;
import com.atlassian.connect.spring.internal.jwt.HttpRequestCanonicalizer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.http.HttpMethod;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(Parameterized.class)
public class JwtQueryHashGeneratorTest {

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Iterable<? extends Object> data() throws IOException {
        URL resource = JwtQueryHashGeneratorTest.class.getResource("/jwt-signed-urls.json");
        SignedUrls signedUrls = new ObjectMapper().readValue(resource, SignedUrls.class);
        return signedUrls.tests;
    }

    private static final String BASE_URL = "https://www.example.com";

    private static final HttpMethod REQUEST_METHOD = HttpMethod.GET;

    @Parameterized.Parameter
    public SignedUrlTest signedUrlTest;

    private JwtQueryHashGenerator queryHashGenerator = new JwtQueryHashGenerator();

    @Test
    public void test() throws URISyntaxException, UnsupportedEncodingException {
        CanonicalHttpRequest canonicalHttpRequest = queryHashGenerator.createCanonicalHttpRequest(REQUEST_METHOD, new URI(signedUrlTest.signedUrl), BASE_URL);
        assertThat(HttpRequestCanonicalizer.canonicalize(canonicalHttpRequest), is(signedUrlTest.canonicalUrl));
    }

    private static class SignedUrls {

        @JsonProperty
        private String secret;

        @JsonProperty
        private List<SignedUrlTest> tests;

        @JsonProperty
        private String comment;
    }

    private static class SignedUrlTest {

        @JsonProperty
        private String name;

        @JsonProperty
        private String canonicalUrl;

        @JsonProperty
        private String signedUrl;

        @Override
        public String toString() {
            return name;
        }
    }
}
