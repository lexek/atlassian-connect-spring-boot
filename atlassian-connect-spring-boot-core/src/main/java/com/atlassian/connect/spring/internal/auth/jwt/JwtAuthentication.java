package com.atlassian.connect.spring.internal.auth.jwt;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.jwt.HttpRequestCanonicalizer;
import com.atlassian.connect.spring.internal.jwt.JwtInvalidClaimException;
import com.nimbusds.jwt.JWTClaimsSet;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;

/**
 * An authentication object representing a verified and accepted JSON Web Token.
 *
 * @see JwtAuthenticationToken
 */
public class JwtAuthentication implements Authentication {

    public static final String ROLE_JWT = "ROLE_JWT";
    public static final String CONTEXT_QSH = "context-qsh";

    private final AtlassianHostUser hostUser;
    private final JWTClaimsSet claims;
    private final String queryStringHash;

    public JwtAuthentication(AtlassianHostUser hostUser, JWTClaimsSet claims, String queryStringHash) {
        this.hostUser = hostUser;
        this.claims = claims;
        this.queryStringHash = queryStringHash;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(ROLE_JWT));
    }

    @Override
    public Object getCredentials() {
        return claims;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return hostUser;
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    @Override
    @SuppressWarnings("deprecation")
    public String getName() {
        StringBuilder nameBuilder = new StringBuilder();
        nameBuilder.append(hostUser.getHost().getClientKey());

        hostUser.getUserAccountId().ifPresent(userAccountId -> nameBuilder.append(String.format(" (%s)", userAccountId)));
        hostUser.getUserKey().ifPresent(userKey -> nameBuilder.append(String.format(" (%s)", userKey)));

        return nameBuilder.toString();
    }

    public void validateQueryStringHash(boolean checkValues) throws JwtInvalidClaimException {
        Object qshClaim = claims.getClaim(HttpRequestCanonicalizer.QUERY_STRING_HASH_CLAIM_NAME);
        if (qshClaim == null) {
            // for the time being, we will allow the qsh claim to be absent and allow such requests
            // through.
            // throw new JwtInvalidClaimException(String.format("Expecting claim '%s' to be present",
            //        HttpRequestCanonicalizer.QUERY_STRING_HASH_CLAIM_NAME));
            return;
        }

        if (checkValues) {
            if (!queryStringHash.equals(qshClaim)) {
                throw new JwtInvalidClaimException(String.format("Expecting claim '%s' to have value '%s' but instead it has the value '%s'",
                        HttpRequestCanonicalizer.QUERY_STRING_HASH_CLAIM_NAME, queryStringHash, qshClaim));
            }
        } else if (!qshClaim.equals(CONTEXT_QSH)) {
            // If we are not checking for a qsh value match we check that the special contextJWT value is
            // present
            throw new JwtInvalidClaimException(String.format("Expecting claim '%s' to have contextJwt value '%s' but instead it has the value '%s'",
                    HttpRequestCanonicalizer.QUERY_STRING_HASH_CLAIM_NAME, CONTEXT_QSH, qshClaim));
        }

    }
}
