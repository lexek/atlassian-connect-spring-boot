package com.atlassian.connect.spring.internal.auth.asymmetric;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.auth.AbstractConnectAuthenticationProvider;
import com.atlassian.connect.spring.internal.auth.jwt.InvalidJwtException;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptor;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.AbstractJwtReader;
import com.atlassian.connect.spring.internal.jwt.JwtInvalidSigningAlgorithmException;
import com.atlassian.connect.spring.internal.jwt.JwtParseException;
import com.atlassian.connect.spring.internal.jwt.JwtVerificationException;
import com.atlassian.connect.spring.internal.jwt.RsaJwtReader;
import com.nimbusds.jwt.JWTClaimsSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.net.URI;
import java.util.Optional;
import java.util.function.Supplier;

public class AsymmetricAuthenticationProvider extends AbstractConnectAuthenticationProvider {

    private static final Logger log = LoggerFactory.getLogger(AsymmetricAuthenticationProvider.class);
    private final Supplier<String> publicKeyBaseURISupplier;
    private final RestTemplateBuilder restTemplateBuilder;
    private final AtlassianHostRepository hostRepository;
    private final AsymmetricPublicKeyProvider asymmetricPublicKeyProvider;

    public AsymmetricAuthenticationProvider(AddonDescriptorLoader addonDescriptorLoader,
                                            AtlassianHostRepository hostRepository,
                                            Supplier<String> publicKeyBaseURISupplier,
                                            RestTemplateBuilder restTemplateBuilder,
                                            AsymmetricPublicKeyProvider asymmetricPublicKeyProvider) {
        super(addonDescriptorLoader, hostRepository);
        this.publicKeyBaseURISupplier = publicKeyBaseURISupplier;
        this.restTemplateBuilder = restTemplateBuilder;
        this.hostRepository = hostRepository;
        this.asymmetricPublicKeyProvider = asymmetricPublicKeyProvider;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        AsymmetricAuthenticationToken pkiAuthentication = (AsymmetricAuthenticationToken) authentication;
        AsymmetricCredentials credentials = pkiAuthentication.getCredentials();

        try {

            String keyId = RsaJwtReader.getKeyIdAndCheckSigningAlgorithm(credentials.getRawJwt());
            String publicKey = asymmetricPublicKeyProvider.fetchPublicKey(publicKeyBaseURISupplier.get(), keyId);

            AbstractJwtReader reader = new RsaJwtReader(publicKey);
            JWTClaimsSet claims = reader.readAndVerify(credentials.getRawJwt(), null);

            return buildAsymmetricAuthentication(credentials, claims);
        } catch (JwtInvalidSigningAlgorithmException e) {
            log.info("Unexpected JWT signing algorithm, most likely due to install request signed with a shared secret", e);
            return null;
        } catch (JwtVerificationException | JwtParseException e) {
            String message = "Could not verify incoming JWT";
            log.warn(message, e);
            throw new InvalidJwtException(message, e);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            String message = "Could initialize RSA verifier";
            log.error(message, e);
            throw new AuthenticationServiceException(message, e);
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(AsymmetricAuthenticationToken.class);
    }

    private Authentication buildAsymmetricAuthentication(AsymmetricCredentials credentials, JWTClaimsSet claims) {
        String audienceClaim = claims.getAudience().stream().findFirst().orElse("");
        AddonDescriptor descriptor = addonDescriptorLoader.getDescriptor();
        URI appBaseUrl = URI.create(descriptor.getBaseUrl());
        if (!appBaseUrl.equals(URI.create(audienceClaim))) {
            String message = "Could not verify incoming JWT due to audience claim mismatch";

            log.warn(String.format("Audience claim '%s' in JWT does not match app baseURL '%s' from the app descriptor", audienceClaim, descriptor.getBaseUrl()));
            throw new InvalidJwtException("Wrong audience found in incoming JWT");
        }

        String hostClientKey = claims.getIssuer();
        Optional<AtlassianHost> host = hostRepository.findById(hostClientKey);
        Optional<AtlassianHostUser> hostUser = host.map(h -> createHostUserFromSubjectClaim(h, claims));
        String qsh = this.computeQueryStringHash(credentials);
        return new AsymmetricAuthentication(claims, hostUser.orElse(null), qsh);
    }
}
