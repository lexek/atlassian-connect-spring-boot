package com.atlassian.connect.spring.internal.auth.jwt;

import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.LifecycleURLHelper;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricAuthentication;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.CanonicalHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * A servlet filter that extracts JSON Web Tokens from the Authorization request header and from the <code>jwt</code>
 * query parameter for use as authentication tokens.
 */
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final String AUTHORIZATION_HEADER_SCHEME_PREFIX = "JWT ";

    private static final String QUERY_PARAMETER_NAME = "jwt";

    private static final Logger log = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

    private final AuthenticationManager authenticationManager;

    private final AddonDescriptorLoader addonDescriptorLoader;

    private final AtlassianConnectProperties atlassianConnectProperties;

    private final AuthenticationFailureHandler failureHandler;

    private final LifecycleURLHelper lifecycleURLHelper;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager,
                                   AddonDescriptorLoader addonDescriptorLoader,
                                   AtlassianConnectProperties atlassianConnectProperties,
                                   ServerProperties serverProperties, LifecycleURLHelper lifecycleURLHelper) {
        this.authenticationManager = authenticationManager;
        this.addonDescriptorLoader = addonDescriptorLoader;
        this.atlassianConnectProperties = atlassianConnectProperties;
        this.failureHandler = createFailureHandler(serverProperties);
        this.lifecycleURLHelper = lifecycleURLHelper;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Optional<String> optionalJwt = getJwtFromRequest(request);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(AsymmetricAuthentication.ROLE_ASYMMETRIC_JWT))) {
            // Already authenticated using Asymmetric algorithm
            log.debug("JWT was asymmetrically signed, skipping symmetric authentication");
            filterChain.doFilter(request, response);
            return;
        } else if (lifecycleURLHelper.isRequestToLifecycleURL(request) && !atlassianConnectProperties.isAllowSymmetricAuthInstallCallback()) {
            if (!addonDescriptorLoader.getDescriptor().getSignedInstall()) {
                String message = "'signed-install' is disabled from the app descriptor, but symmetric lifecycle authentication is not allowed";
                log.error(message);
                failureHandler.onAuthenticationFailure(request, response, new InvalidInstallHookConfigurationException(message));
                return;
            } else {
                String message = "Fallback authentication for install hook is disabled.";
                log.error(message);
                failureHandler.onAuthenticationFailure(request, response, new InvalidSymmetricJwtException(message));
                return;
            }

        }

        if (optionalJwt.isPresent()) {
            Authentication authenticationRequest = createJwtAuthenticationToken(request, optionalJwt.get());

            Authentication authenticationResult;
            try {
                authenticationResult = authenticationManager.authenticate(authenticationRequest);
                SecurityContextHolder.getContext().setAuthentication(authenticationResult);
            } catch (ProviderNotFoundException e) {
                log.info("Could not authenticate the request with symmetric signature");
            } catch (AuthenticationException e) {
                log.warn("Failed to authenticate request", e);
                if (shouldIgnoreInvalidJwt(request, e)) {
                    log.warn("Received JWT authentication from unknown host ({}), but allowing anyway",
                            ((UnknownJwtIssuerException) e).getIssuer());
                } else {
                    failureHandler.onAuthenticationFailure(request, response, e);
                    return;
                }
            }
        }
        filterChain.doFilter(request, response);
    }

    private SimpleUrlAuthenticationFailureHandler createFailureHandler(ServerProperties serverProperties) {
        SimpleUrlAuthenticationFailureHandler failureHandler = new SimpleUrlAuthenticationFailureHandler(serverProperties.getError().getPath());
        failureHandler.setAllowSessionCreation(false);
        failureHandler.setUseForward(true);
        return failureHandler;
    }

    private static Optional<String> getJwtFromRequest(HttpServletRequest request) {
        Optional<String> optionalJwt = getJwtFromHeader(request);
        if (!optionalJwt.isPresent()) {
            optionalJwt = getJwtFromParameter(request);
        }
        return optionalJwt;
    }

    private static Optional<String> getJwtFromHeader(HttpServletRequest request) {
        Optional<String> optionalJwt = Optional.empty();
        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (!StringUtils.isEmpty(authHeader) && authHeader.startsWith(AUTHORIZATION_HEADER_SCHEME_PREFIX)) {
            String jwt = authHeader.substring(AUTHORIZATION_HEADER_SCHEME_PREFIX.length());
            optionalJwt = Optional.of(jwt);
        }

        return optionalJwt;
    }

    private static Optional<String> getJwtFromParameter(HttpServletRequest request) {
        Optional<String> optionalJwt = Optional.empty();
        String jwt = request.getParameter(QUERY_PARAMETER_NAME);
        if (!StringUtils.isEmpty(jwt)) {
            optionalJwt = Optional.of(jwt);
        }
        return optionalJwt;
    }

    private JwtAuthenticationToken createJwtAuthenticationToken(HttpServletRequest request, String jwt) {
        log.debug("Retrieved JWT from request");
        CanonicalHttpServletRequest canonicalHttpServletRequest = new CanonicalHttpServletRequest(request);
        JwtCredentials credentials = new JwtCredentials(jwt, canonicalHttpServletRequest);
        return new JwtAuthenticationToken(credentials);
    }

    private boolean shouldIgnoreInvalidJwt(HttpServletRequest request, AuthenticationException e) {
        return e instanceof UnknownJwtIssuerException
                && ((lifecycleURLHelper.isRequestToInstalledLifecycle(request) && atlassianConnectProperties.isAllowReinstallMissingHost())
                || lifecycleURLHelper.isRequestToUninstalledLifecycle(request));
    }

}
