package com.atlassian.connect.spring.internal.auth.asymmetric;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;

public class AsymmetricAuthenticationToken extends AbstractAuthenticationToken {

    private AsymmetricCredentials credentials;


    public AsymmetricAuthenticationToken(AsymmetricCredentials credentials) {
        super(Collections.emptySet());
        this.credentials = credentials;
    }

    public AsymmetricAuthenticationToken(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    @Override
    public AsymmetricCredentials getCredentials() {
        return credentials;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }
}
