package com.atlassian.connect.spring.internal.auth;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.ContextJwt;
import com.atlassian.connect.spring.IgnoreJwt;
import com.atlassian.connect.spring.internal.auth.jwt.JwtAuthentication;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.JwtInvalidClaimException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;

/**
 * A handler interceptor that enforces JWT authentication for all handler methods not annotated with {@link IgnoreJwt}.
 */
@Component
public class RequireAuthenticationHandlerInterceptor extends HandlerInterceptorAdapter {

    private static final Logger log = LoggerFactory.getLogger(RequireAuthenticationHandlerInterceptor.class);

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (isApplicableDispatcherType(request)) {
            if (handlerRequiresJwtAuthentication(handler)) {
                if (!requestIsSigned()) {
                    log.info("Rejected incoming request for controller requiring JWT authentication ({} {})", request.getMethod(), request.getRequestURI());
                    response.addHeader(HttpHeaders.WWW_AUTHENTICATE, String.format("JWT realm=\"%s\"", addonDescriptorLoader.getDescriptor().getKey()));
                    response.sendError(HttpStatus.UNAUTHORIZED.value());
                    return false;
                }

                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (authentication instanceof JwtAuthentication) {
                    JwtAuthentication jwtAuthentication = (JwtAuthentication) authentication;
                    try {
                        jwtAuthentication.validateQueryStringHash(handlerRequiresQshValidation(handler));
                    } catch (JwtInvalidClaimException e) {
                        // qsh mismatch
                        response.sendError(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
                        return false;
                    }
                }
            }
        }
        return true;
    }


    private boolean handlerRequiresJwtAuthentication(Object handler) {
        return handler instanceof HandlerMethod && !handlerHasAnnotation((HandlerMethod) handler, IgnoreJwt.class);
    }

    private boolean handlerRequiresQshValidation(Object handler) {
        return handler instanceof HandlerMethod && !handlerHasAnnotation((HandlerMethod) handler, ContextJwt.class);
    }

    private <T extends Annotation> boolean handlerHasAnnotation(HandlerMethod method, Class<T> annotationClass) {
        return method.getMethod().isAnnotationPresent(annotationClass)
                || method.getBeanType().isAnnotationPresent(annotationClass);
    }

    private boolean requestIsSigned() {
        boolean signed = false;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            signed = authentication.isAuthenticated() && (authentication.getPrincipal() instanceof AtlassianHostUser);
        }
        return signed;
    }

    private boolean isApplicableDispatcherType(HttpServletRequest request) {
        DispatcherType dispatcherType = request.getDispatcherType();
        return !dispatcherType.equals(DispatcherType.ASYNC) && !dispatcherType.equals(DispatcherType.ERROR);
    }
}
