package com.atlassian.connect.spring.internal.auth.jwt;

import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricAuthenticationProvider;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricPublicKeyProvider;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.SecurityConfigurer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * A configuration class responsible for registering a {@link JwtAuthenticationProvider} without interfering with
 * auto-configured Basic authentication support.
 */
@Configuration
@Order(Ordered.LOWEST_PRECEDENCE)
public class JwtGlobalAuthenticationConfigurerAdapter extends GlobalAuthenticationConfigurerAdapter {

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Autowired
    private AtlassianHostRepository hostRepository;

    @Autowired
    private SecurityProperties securityProperties;

    @Autowired
    private ObjectProvider<PasswordEncoder> passwordEncoder;

    @Autowired
    private AtlassianConnectProperties properties;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Autowired
    private AsymmetricPublicKeyProvider asymmetricPublicKeyProvider;

    @Override
    public void init(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        JwtAuthenticationProvider jwtAuthenticationProvider = new JwtAuthenticationProvider(addonDescriptorLoader, hostRepository);
        authenticationManagerBuilder.apply(new JwtSecurityConfigurer<>(jwtAuthenticationProvider));

        AsymmetricAuthenticationProvider asymmetricAuthenticationProvider =
                new AsymmetricAuthenticationProvider(addonDescriptorLoader, hostRepository, () -> properties.getPublicKeyBaseUrl(), restTemplateBuilder, asymmetricPublicKeyProvider);
        authenticationManagerBuilder.apply(new JwtSecurityConfigurer<>(asymmetricAuthenticationProvider));

        UserDetailsServiceAutoConfiguration userDetailsServiceAutoConfiguration = new UserDetailsServiceAutoConfiguration();
        authenticationManagerBuilder.userDetailsService(userDetailsServiceAutoConfiguration.inMemoryUserDetailsManager(securityProperties, passwordEncoder));
    }

    private static class JwtSecurityConfigurer<T extends AuthenticationProvider> implements SecurityConfigurer<AuthenticationManager, AuthenticationManagerBuilder> {

        private final T authenticationProvider;

        public JwtSecurityConfigurer(T authenticationProvider) {
            this.authenticationProvider = authenticationProvider;
        }

        @Override
        public void init(AuthenticationManagerBuilder builder) throws Exception {}

        @Override
        public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
            authenticationManagerBuilder.authenticationProvider(authenticationProvider);
        }
    }

}
