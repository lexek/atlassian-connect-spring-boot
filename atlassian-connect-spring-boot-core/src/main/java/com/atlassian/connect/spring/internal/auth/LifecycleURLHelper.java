package com.atlassian.connect.spring.internal.auth;

import com.atlassian.connect.spring.internal.descriptor.AddonDescriptor;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.Objects;

@Component
public class LifecycleURLHelper {

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    public boolean isRequestToLifecycleURL(HttpServletRequest request) {
        return isRequestToInstalledLifecycle(request) || isRequestToUninstalledLifecycle(request);
    }

    public boolean isRequestToInstalledLifecycle(HttpServletRequest request) {
        AddonDescriptor descriptor = addonDescriptorLoader.getDescriptor();
        String url = descriptor.getBaseUrl() + descriptor.getInstalledLifecycleUrl();
        return isRequestToUrl(request, url);
    }

    public boolean isRequestToUninstalledLifecycle(HttpServletRequest request) {
        AddonDescriptor descriptor = addonDescriptorLoader.getDescriptor();
        String url = descriptor.getBaseUrl() + descriptor.getUninstalledLifecycleUrl();
        return isRequestToUrl(request, url);
    }

    private boolean isRequestToUrl(HttpServletRequest request, String url) {
        UriComponents urlComponents = UriComponentsBuilder.fromUri(URI.create(url)).build();
        UriComponents requestComponents = UriComponentsBuilder.fromUri(URI.create(request.getRequestURL().toString()))
                .query(request.getQueryString()).build();
        return Objects.equals(requestComponents.getPath(), urlComponents.getPath())
                && requestComponents.getQueryParams().entrySet().containsAll(urlComponents.getQueryParams().entrySet());
    }
}
