package com.atlassian.connect.spring.internal.auth.jwt;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * An authentication exception thrown when descriptor 'signed-install' configuration conflicts with 'allow-symmetric-auth-install-callback' system config
 */
@ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "Both 'signed-install' and 'allow-symmetric-auth-install-callback' were disabled")
public class InvalidInstallHookConfigurationException extends AuthenticationException {
    public InvalidInstallHookConfigurationException(String message) {
        super(message);
    }
}
