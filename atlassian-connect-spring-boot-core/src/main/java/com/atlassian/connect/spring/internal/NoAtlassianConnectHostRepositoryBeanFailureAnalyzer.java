package com.atlassian.connect.spring.internal;

import com.atlassian.connect.spring.AtlassianHostRepository;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.boot.diagnostics.AbstractFailureAnalyzer;
import org.springframework.boot.diagnostics.FailureAnalysis;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

@Order(Ordered.HIGHEST_PRECEDENCE)
public class NoAtlassianConnectHostRepositoryBeanFailureAnalyzer extends AbstractFailureAnalyzer<NoSuchBeanDefinitionException> {

    private static final String DESCRIPTION = "atlassian-connect-spring-boot requires a bean of type 'com.atlassian.connect.spring.AtlassianHostRepository' that could not be found. This repository is used for storing installation data.";

    private static final String ACTION = "Choose a Spring Data implementation to use with AtlassianHostRepository, and enable Repository scanning with the appropriate @Enable${store}Repositories annotation. If you choose Spring Data JPA, consider using atlassian-connect-spring-boot-jpa-starter.";

    @Override
    protected FailureAnalysis analyze(Throwable rootFailure, NoSuchBeanDefinitionException cause) {
        FailureAnalysis analysis = null;
        if (AtlassianHostRepository.class.equals(cause.getBeanType())) {
            analysis = new FailureAnalysis(DESCRIPTION, ACTION, cause);
        }
        return analysis;
    }
}
