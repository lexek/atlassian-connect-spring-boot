package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.auth.AtlassianConnectSecurityContextHelper;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * A provider of a {@link RestTemplate} that signs requests to Atlassian hosts with JSON Web Tokens.
 */
@Component
public class JwtSigningRestTemplateFactory {

    private RestTemplateBuilder restTemplateBuilder;
    private final JwtGenerator jwtGenerator;
    private final String atlassianConnectClientVersion;
    private RestTemplate restTemplate;

    @Autowired
    public JwtSigningRestTemplateFactory(RestTemplateBuilder restTemplateBuilder,
            JwtGenerator jwtGenerator,
            @Value("${atlassian.connect.client-version}") String atlassianConnectClientVersion,
            AtlassianHostUriResolver hostUriResolver,
            AtlassianConnectSecurityContextHelper securityContextHelper) {
        this.restTemplateBuilder = restTemplateBuilder;
        this.jwtGenerator = jwtGenerator;
        this.atlassianConnectClientVersion = atlassianConnectClientVersion;
        JwtSigningClientHttpRequestInterceptor requestInterceptor = new JwtSigningClientHttpRequestInterceptor(
                jwtGenerator, atlassianConnectClientVersion, hostUriResolver, securityContextHelper);
        restTemplate = restTemplateBuilder.additionalInterceptors(requestInterceptor).build();
    }

    public RestTemplate getJwtRestTemplate() {
        return restTemplate;
    }

    @Cacheable(value = {"jwt-clients"})
    public RestTemplate getJwtRestTemplate(AtlassianHost host) {
        JwtSigningClientHttpRequestInterceptor requestInterceptor = new JwtSigningClientHttpRequestInterceptor(
                jwtGenerator, atlassianConnectClientVersion, host);
        return restTemplateBuilder.additionalInterceptors(requestInterceptor).build();
    }
}
