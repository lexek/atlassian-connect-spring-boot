package com.atlassian.connect.spring.internal.request;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

/**
 * A helper class for resolving URLs relative to the base URL of an {@link AtlassianHost}.
 */
@Component
public class AtlassianHostUriResolver {

    private static final Logger log = LoggerFactory.getLogger(AtlassianHostUriResolver.class);

    private AtlassianHostRepository hostRepository;

    @Autowired
    public AtlassianHostUriResolver(AtlassianHostRepository hostRepository) {
        this.hostRepository = hostRepository;
    }

    public static boolean isRequestToHost(URI requestUri, AtlassianHost host) {
        URI hostBaseUri = URI.create(host.getBaseUrl());
        return !hostBaseUri.relativize(requestUri).isAbsolute();
    }

    public static String getBaseUrl(URI uri) {
        try {
            return new URI(uri.getScheme(), uri.getAuthority(), null, null, null).toString();
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
        }
    }

    public Optional<AtlassianHost> getHostFromRequestUrl(URI uri) {
        Optional<AtlassianHost> optionalHost = Optional.empty();
        if (uri.isAbsolute()) {
            optionalHost = getHostFromBaseUrl(AtlassianHostUriResolver.getBaseUrl(uri));
            if (!optionalHost.isPresent()) {
                optionalHost = getHostFromBaseUrl(getBaseUrlWithFirstPathElement(uri));
            }
        }

        if (optionalHost.isPresent()) {
            log.warn("Host look-up by base URL is deprecated");
        }

        return optionalHost;
    }

    private Optional<AtlassianHost> getHostFromBaseUrl(String baseUrl) {
        return hostRepository.findFirstByBaseUrlOrderByLastModifiedDateDesc(baseUrl);
    }

    private String getBaseUrlWithFirstPathElement(URI uri) {
        try {
            return new URI(uri.getScheme(), uri.getAuthority(), getFirstPathElement(uri), null, null).toString();
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
        }
    }

    private String getFirstPathElement(URI uri) {
        String path = uri.getPath();
        if (path != null ) {
            int secondSlashIndex = path.indexOf('/', 1);
            if (secondSlashIndex != -1) {
                path = path.substring(0, secondSlashIndex);
            }
        }
        return path;
    }
}
