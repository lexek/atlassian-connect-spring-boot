package com.atlassian.connect.spring;

import org.springframework.http.HttpMethod;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

/**
 * A helper class for obtaining preconfigured {@link RestTemplate}s to make authenticated requests to Atlassian hosts.
 *
 * <h3>JWT</h3>
 *
 * <p>To make requests using JWT, the add-on must specify the authentication type {@code jwt} in its add-on descriptor.
 *
 * <p>To obtain a {@code RestTemplate} using JWT authentication, use {@link #authenticatedAsAddon()}:
 * <blockquote><pre><code> &#64;Autowired
 * private AtlassianHostRestClients restClients;
 *
 * public void makeRequest() {
 *     restClients.authenticatedAsAddon().getForObject(...);
 * }</code></pre></blockquote>
 *
 * <h3>OAuth 2.0 - JWT Bearer Token</h3>
 *
 * <p>To make requests using OAuth 2.0, the add-on must request the {@code ACT_AS_USER} scope in its add-on descriptor.
 *
 * <p>To obtain a {@code RestTemplate} using OAuth 2.0 authentication, use {@link #authenticatedAsHostActor} or
 * {@link #authenticatedAs(AtlassianHostUser)}:
 * <blockquote><pre><code> &#64;Autowired
 * private AtlassianHostRestClients restClients;
 *
 * public void makeRequest() {
 *     restClients.authenticatedAsHostActor().getForObject(...);
 * }</code></pre></blockquote>
 *
 * @since 1.1.0
 */
public interface AtlassianHostRestClients {

    /**
     * Returns a {@code RestTemplate} for making requests to Atlassian hosts using JWT authentication.
     * The principal of the request is the add-on.
     *
     * <p>During processing of a request from an Atlassian host, relative URLs can be used to make requests to the
     * current host.
     *
     * <p>When a request is made to an absolute URL, the request URL is used to resolve the destination Atlassian host.
     * If no host matches, the request is not signed.
     *
     * @return a REST client for JWT authentication
     * @see #authenticatedAsAddon(AtlassianHost)
     */
    RestTemplate authenticatedAsAddon();

    /**
     * Returns a {@code RestTemplate} for making requests to Atlassian hosts using JWT authentication.
     * The principal of the request is the add-on.
     *
     * <p>Relative URLs can be used to make requests to the given host.
     *
     * <p>When a request is made to an absolute URL, the URL must match the base URL of the given host.
     *
     * @param host the host to which the request should be made
     * @return a REST client for JWT authentication
     * @see #authenticatedAsAddon()
     */
    RestTemplate authenticatedAsAddon(AtlassianHost host);

    /**
     * Creates a JSON Web Token for use when the {@code RestTemplate} provided by {@link #authenticatedAsAddon()}
     * cannot be used to make requests to Atlassian hosts, such as when using Jersey.
     * <blockquote><pre><code> WebTarget webTarget = ClientBuilder.newClient().target(host.getBaseUrl()).path(...);
     * String jwt = atlassianHostRestClients.createJwt(HttpMethod.GET, webTarget.getUri());
     * Response response = webTarget.request().header("Authorization", "JWT " + jwt).get();</code></pre></blockquote>
     *
     * <p><strong>NOTE:</strong> Whenever possible, use of {@link #authenticatedAsAddon()} is recommended over use of
     * this method.
     *
     * <p>The created JWT is restricted for use with the given HTTP method and request URL.
     *
     * <p>The request URL is used to resolve the destination Atlassian host. If no host matches, an
     * {@code IllegalArgumentException} is thrown.
     *
     * @param method the HTTP method of the request to be authenticated
     * @param uri the absolute URL of the request to be authenticated
     * @return a JWT for use when authenticating as the add-on
     * @throws IllegalArgumentException if the URL did not have the base URL of any installed host
     * @see #authenticatedAsAddon()
     * @see #authenticatedAsAddon(AtlassianHost)
     * @since 1.3.0
     */
    String createJwt(HttpMethod method, URI uri);

    /**
     * Returns a {@code RestTemplate} for making requests to the currently authenticated Atlassian host using
     * OAuth 2.0 JWT Bearer Token authentication. The principal of the request is the currently authenticated user.
     *
     * <p>On first invocation, the {@code OAuth2RestTemplate} will request an access token from Atlassian's
     * authorization server. The access token will then be cached for further use. Once the token has expired, a new
     * token will be fetched transparently. Additionally, the {@code OAuth2RestTemplate} for a particular host user
     * is cached between requests using
     * <a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/html/cache.html">Spring Caching</a>.
     *
     * @return a REST client for OAuth 2.0 JWT Bearer Token authentication
     * @see <a href="https://projects.spring.io/spring-security-oauth/docs/oauth2.html">Spring Security OAuth 2</a>
     * @see #authenticatedAs(AtlassianHostUser)
     */
    OAuth2RestTemplate authenticatedAsHostActor();

    /**
     * Returns a {@code RestTemplate} for making requests to the given Atlassian host using OAuth 2.0 JWT Bearer Token
     * authentication. The principal of the request is the given user.
     *
     * <p>On first invocation, the {@code OAuth2RestTemplate} will request an access token from Atlassian's
     * authorization server. The access token will then be cached for further use. Once the token has expired, a new
     * token will be fetched transparently. Additionally, the {@code OAuth2RestTemplate} for a particular host user
     * is cached between requests using
     * <a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/html/cache.html">Spring Caching</a>.
     *
     * @param hostUser the host to which the request should be made, and the user principal
     * @return a REST client for OAuth 2.0 JWT Bearer Token authentication
     * @see <a href="https://projects.spring.io/spring-security-oauth/docs/oauth2.html">Spring Security OAuth 2</a>
     * @see #authenticatedAsHostActor()
     */
    OAuth2RestTemplate authenticatedAs(AtlassianHostUser hostUser);
}
