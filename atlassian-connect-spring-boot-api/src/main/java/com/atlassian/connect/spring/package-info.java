/**
 * Provides classes for building <a href="http://connect.atlassian.com/">Atlassian Connect</a> add-ons using
 * <a href="http://projects.spring.io/spring-boot/">Spring Boot</a> and
 * <a href="https://bitbucket.org/atlassian/atlassian-connect-spring-boot"><code>atlassian-connect-spring-boot</code></a>.
 *
 * <p>To use the Spring Boot starter for Atlassian Connect in your application, include the following dependency in
 * your Maven POM:
 * <blockquote><pre><code> &lt;dependency&gt;
 *     &lt;groupId&gt;com.atlassian.connect&lt;/groupId&gt;
 *     &lt;artifactId&gt;atlassian-connect-spring-boot-starter&lt;/artifactId&gt;
 *     &lt;version&gt;${atlassian-connect-spring-boot.version}&lt;/version&gt;
 * &lt;/dependency&gt;</code></pre></blockquote>
 *
 * <h2 id="descriptor">Add-on Descriptor</h2>
 *
 * To enable the functionality provided by this Spring Boot starter, an Atlassian Connect add-on descriptor,
 * <code>atlassian-connect.json</code> must be present at the root of the class path.
 *
 * <h2 id="lifecycle">Add-on Lifecycle</h2>
 *
 * The <a href="http://projects.spring.io/spring-data/">Spring Data</a> repository
 * {@link com.atlassian.connect.spring.AtlassianHostRepository} stores information about Atlassian hosts in which the
 * add-on is or has been installed. A component implementing this interface is required for your application to start.
 *
 * <p>Upon successful completion of add-on installation or uninstallation,
 * A <a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/html/beans.html#context-functionality-events">Spring application event</a>
 * will be fired: {@link com.atlassian.connect.spring.AddonInstalledEvent} or
 * {@link com.atlassian.connect.spring.AddonUninstalledEvent}.
 *
 * <h2 id="authentication-incoming">Authentication of Incoming Requests</h2>
 *
 * During processing of a request from an Atlassian host, the details of the host and of the user at the browser can be
 * obtained from the {@link com.atlassian.connect.spring.AtlassianHostUser}.
 *
 * <p>By default, all <a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/html/mvc.html">Spring Web MVC</a>
 * controllers require authentication by a JSON Web Token issued by an Atlassian host with the add-on installed.
 * The {@link com.atlassian.connect.spring.IgnoreJwt} annotation can be used to bypass that requirement.
 *
 * <h2 id="authentication-outgoing">Authentication of Outgoing Requests</h2>
 *
 * {@link com.atlassian.connect.spring.AtlassianHostRestClients} provides {@code RestTemplate}s for making authenticated
 * requests to Atlassian hosts as the add-on or as a user.
 *
 * <p><b>Deprecated:</b> A {@code RestTemplate} for JSON Web Token authentication is provided as a {@code @Component} and
 * can be {@code @Autowired}.
 *
 * @see <a href="https://developer.atlassian.com/">Atlassian Developers</a>
 * @since 1.0.0
 */
package com.atlassian.connect.spring;
